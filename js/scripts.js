//Change these to match your server information!
var serverip = "minecraft.medievalcraft.de";
var serverport = "25565";

$(document).ready(function(){
    
    /*
        This is a JavaScript file that gets your server's status information and displays
        the player count on your portal.
    */
    
    //Do not touch these or past this point!
    var onlineplayers = 0;
    var maxplayers = 0;
    
    xhr = new XMLHttpRequest();
    var url = "https://mcapi.ca/query/" + serverip + ":" + serverport + "/players";
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4 && xhr.status == 200) {
            var json = JSON.parse(xhr.responseText);
            
            onlineplayers = json.players.online;
            maxplayers = json.players.max;
            
            $(".pure-u-1-1 h1").text("Derzeit sind " + onlineplayers + /*"/" + maxplayers +*/ " Spieler auf MedievalCraft!");
                                                                /* this above part is optional */
                                                                
            $(".pure-u-1-1 h3").text(serverip);
            $("#serverBoxOuter").html("<input id=\"serverBoxIP\" style=\"background: transparent;opacity:0;outline:none;border:none;\" value=" + serverip + "></input>");
        }
    }
    xhr.send();

});


//Copy server ip when user clicks on the ip
function CopyIP() {    
    // find target element
    var inp = document.getElementById('serverBoxIP');

    // is element selectable?
    if (inp && inp.select) {
    
      if($(inp).val() == "Kopiert!") return false;
    
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
        $(inp).val("Kopiert!");
        setTimeout(function () {
            $(inp).val(serverip);        
        }, 1800);
      }
      catch (err) {
        alert('Bitte drücke STRG+C zum Kopieren!');
      }

    }
}